/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package apis.controllers

import akka.actor.{ActorRef, ActorSystem}
import akka.cluster.Cluster
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.Publish
import akka.stream.OverflowStrategy
import akka.stream.scaladsl.Source
import com.typesafe.config.Config
import concept.operators.OperatorManager
import concept.sales.commands.InitiateSaleTransaction
import javax.inject.{Inject, Singleton}
import play.api.Logger
import play.api.http.ContentTypes
import play.api.libs.EventSource
import play.api.libs.EventSource.{EventIdExtractor, EventNameExtractor}
import play.api.libs.json._
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

import scala.util.Random

@Singleton
class SalesController @Inject()(cc: ControllerComponents, config: Config) extends AbstractController(cc) {
  private val actorSystemName = config.getString("akka.actor.system")
  Logger.info(s"Getting actor system : [${actorSystemName}]")

  private implicit val initiateSaleTransactionWrites = Json.writes[InitiateSaleTransaction]

  private val saleSystemName = config.getString("apps.sales.system")
  private implicit val actorSystem = ActorSystem(saleSystemName, config)

  println(s"ActorSystem => ${actorSystem}")
  Cluster.get(actorSystem).getSelfRoles.forEach( role => println(s"Role => `${role}`"))

  private val operatorManager = actorSystem.actorOf(OperatorManager.props(), "operator-manager")
  private val mediator = DistributedPubSub(actorSystem).mediator
  private val salesRequestTopic = config.getString("apps.sales.request-topic")
  private val bufferSize = 10

  private var saleId = new Random(Int.MaxValue / 598793).nextInt()
  
  /**
    * Post transactions to Sale system
    *
    * @param operator
    * @return
    */
  def initiateRequests(operator: String): Action[AnyContent] = Action { implicit request =>
    saleId += 1
    val saleRef = if (saleId % 2 == 0) {
      s"Initiated an Even payment: Sale ${saleId}"
    } else {
      s"Initiated an Odd payment: Sale ${saleId}"
    }

    val msg = InitiateSaleTransaction(saleId, operator, saleRef)
    mediator ! Publish(salesRequestTopic, msg)
    Logger.info(
      s"""
         | The operator is ${operator}.........
         | Sale reference : [${saleRef}]
         | Mediator reference : [${mediator.path}]
       """.stripMargin)

    Created(Json.toJson(msg)).as("application/json")
  }

  /**
    * Enable SSE for the responses for operator
    *
    * @param operator
    * @return
    */
  def streamResponses(operator: String): Action[AnyContent] = Action { implicit request =>
    Logger.info(s"The operator is ${operator}.........")
    val responses: Source[JsValue, ActorRef] = Source.actorRef[JsValue](bufferSize, OverflowStrategy.fail)
      .mapMaterializedValue { ref =>
        operatorManager ! OperatorManager.Commands.Subscribe(operator, ref)
        ref
      }

    implicit val eventNameExtractor = EventNameExtractor[JsValue] (_ => Some("SaleToPOIResponse"))
    implicit val eventIdExtractor = EventIdExtractor[JsValue](data => (data \ "poiTransactionId").asOpt[String])
    Ok.chunked(responses via EventSource.flow).as(ContentTypes.EVENT_STREAM)
  }

  def echos(): Action[AnyContent] = Action { implicit request =>
    val data: String = request.body.asJson.get.toString()
    Ok(data).as("application/json")
  }
}
