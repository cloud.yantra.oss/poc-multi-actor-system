/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

import akka.actor.ActorSystem
import akka.cluster.Cluster
import akka.cluster.sharding.ClusterSharding
import com.typesafe.config.ConfigFactory
import concept.pois.PoiDevice
import concept.shards.PoiDeviceShard

object PoiActorSystemApp extends App {
  val config = ConfigFactory.load()
  val actorSystem = ActorSystem.create(config.getString("akka.actor.system"), config)
  println(s"ActorSystem => ${actorSystem}")
  Cluster.get(actorSystem).getSelfRoles.forEach(role => println(s"Role => `${role}`"))

  println(s"Starting cluster sharding for Poi Devices")
  val poiDeviceShard = PoiDeviceShard.startPoiDeviceSharding(actorSystem, PoiDevice.props)

  println(s"All shards types in the cluster are ......................")
  ClusterSharding(actorSystem).shardTypeNames.foreach{ t =>
    println(s"Avaliable shard type is [${t}]")
  }
  println(s" Path to shard region [${ClusterSharding(actorSystem).shardRegion("PoiDevice").path}]")

}
