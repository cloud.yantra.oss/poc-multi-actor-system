/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.pois

import java.time.LocalDateTime
import java.util.UUID

import akka.actor.{ActorLogging, Props, ReceiveTimeout}
import akka.cluster.sharding.ShardRegion.Passivate
import akka.persistence.PersistentActor
import concept.common.SafeShutdown
import concept.pois.commands.PaymentTransactionRequest
import concept.pois.events.{PaymentTransactionFailed, PaymentTransactionProcessed, PoiEvent}

//class PoiDevice(poiId: String) extends PersistentActor with ActorLogging {
class PoiDevice extends PersistentActor with ActorLogging {

  private var transactionsProcessed = 0
  private var transactionRejected = 0

  override def receiveRecover: Receive = {
    case msg => log.debug(s"Receive recover for Poid [${persistenceId}]...............[${msg.getClass}]")
  }

  override def receiveCommand: Receive = {
    case Tuple2(saleSystem, payment@PaymentTransactionRequest(transactionId, operator, destinationPoiId, paymentData)) =>
      if ( ! checkPoiId( destinationPoiId)) {
        val failure = PaymentTransactionFailed(transactionId,
          operator,
          destinationPoiId,
          paymentData,
          s"PoiDevice with id ${persistenceId}, at ${self.path.address}, can not process on behalf ${destinationPoiId}")
          log.info(s"PoiDevice with id ${persistenceId}, at ${self.path.address}, can not process on behalf ${destinationPoiId}")
        persist(failure)(trackProcessingCount)
        sender ! Tuple2(saleSystem, failure)
      } else {
        log.info(s"PoiDevice with id ${persistenceId}, at ${self.path.address}, processed successfully payment -> ${payment}")
        val success = PaymentTransactionProcessed(transactionId,
          operator,
          destinationPoiId,
          paymentData,
          UUID.randomUUID().toString,
          LocalDateTime.now()
        )
        persist(success)(trackProcessingCount)
        sender ! Tuple2(saleSystem, success)
      }

    case ReceiveTimeout =>
      context.parent ! Passivate(stopMessage = SafeShutdown)

    case SafeShutdown =>
      context.stop(self)

    case _ =>
      log.warning(s"C'mon send PoiId [${persistenceId}] some proper messages.")
      self ! SafeShutdown
  }

  //  override def persistenceId: String = poiId
  override def persistenceId: String = self.path.name.split("/").toList.last

  private def trackProcessingCount: PoiEvent => Unit = {
    case _: PaymentTransactionProcessed => transactionsProcessed += 1
    case _: PaymentTransactionFailed => transactionRejected += 1
  }

  private def checkPoiId(destinationId: String): Boolean = destinationId == persistenceId
}

object PoiDevice {
  //  def props(poiId: String): Props = Props(new PoiDevice(poiId))
  def props: Props = Props(new PoiDevice())
}
