/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.pois

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import concept.common.SafeShutdown
import concept.pois.commands.PaymentTransactionRequest
import concept.pois.events.{PaymentTransactionFailed, PaymentTransactionProcessed}
import concept.shards.PoiDeviceShard

class PoiManager extends Actor with ActorLogging {
  override def receive: Receive = {

    case Tuple2(origin: ActorRef, ptr@PaymentTransactionRequest(_, _, _, _)) =>
      //      val poiDevice = getPoiDevice(poiId)
      //      poiDevice ! Tuple2(origin, ptr)
      PoiDeviceShard.startPoiDeviceSharding(context.system, PoiDevice.props) ! Tuple2(origin, ptr)
    case Tuple2(origin: ActorRef, success: PaymentTransactionProcessed) =>
      origin ! success

    case Tuple2(origin: ActorRef, failure: PaymentTransactionFailed) =>
      origin ! failure

    case SafeShutdown =>
      context.stop(self)

    case obj@_ =>
      log.warning(s"Unrecognized message -> ${obj.getClass.getCanonicalName}")
  }

}

object PoiManager {
  def props: Props = Props(new PoiManager)
}


