/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package controllers

import java.util.UUID

import akka.actor.{ActorRef, ActorSystem}
import akka.stream.OverflowStrategy
import akka.stream.scaladsl.Source
import concept.operators.OperatorManager
import javax.inject.{Inject, Singleton}
import play.api.http.ContentTypes
import play.api.libs.EventSource
import play.api.libs.json.JsValue
import play.api.mvc._

import scala.concurrent.duration._
import scala.language.postfixOps

@Singleton
class ServerSideEventController @Inject()(cc: ControllerComponents)
  extends AbstractController(cc) {

  implicit val actorSystem = ActorSystem("sale-system")
  private val operatorManager = actorSystem.actorOf(OperatorManager.props(), "operator-manager")

  private val bufferSize = 10
  private val initialDelay = 0 millis
  private val interval = 1000 millis
  private val tick = "tick"

  def streamUuids(): Action[AnyContent] = Action {
    Ok.chunked(uuidSource via EventSource.flow).as(ContentTypes.EVENT_STREAM)
  }

  def actorBasedEvents(operator: String): Action[AnyContent] = Action {
    println(operator)
    Ok.chunked(saleResponses(operator) via EventSource.flow).as(ContentTypes.EVENT_STREAM)
  }

  private def saleResponses(operator: String): Source[JsValue, ActorRef] =
    Source.actorRef[JsValue](bufferSize, OverflowStrategy.fail)
      .mapMaterializedValue { ref =>
        operatorManager ! OperatorManager.Commands.Subscribe(operator, ref)
        ref
      }

  private def uuidSource: Source[String, _] = {
    val tickSource = Source.tick(initialDelay, interval, tick)
    tickSource.map { (_) =>
      val uuid = UUID.randomUUID().toString
      println(s"Sending a new UUID : [${uuid}]")
      uuid
    }
  }

}
