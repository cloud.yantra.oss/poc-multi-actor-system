/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package controllers

import javax.inject.Inject
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

class HomeController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  def index(): Action[AnyContent] = Action {
    Ok(views.html.index())
  }

  def displayServerSentEvents(): Action[AnyContent] = Action {
    Ok(views.html.serverSentEvents())
  }

  def displayEventsForOperator(operator: String): Action[AnyContent] = Action {
    Ok(views.html.remoteServerSentEvents(operator))
  }
}
