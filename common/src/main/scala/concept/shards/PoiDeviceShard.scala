/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.shards

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.cluster.sharding.{ClusterSharding, ClusterShardingSettings, ShardRegion}
import concept.pois.commands.PaymentTransactionRequest

object PoiDeviceShard {

  def startPoiDeviceSharding(system: ActorSystem, props: Props): ActorRef = {
    ClusterSharding(system)
      .start(
        typeName = "PoiDevice",
        entityProps = props,
        settings = ClusterShardingSettings(system).withRole("poi-system"),
        extractEntityId = extractEntityId,
        extractShardId = extractShardId)
  }

  def startPoiDeviceShardingProxy(system: ActorSystem): ActorRef = {
    ClusterSharding(system)
      .startProxy(
        typeName = "PoiDevice",
        role = Some("poi-system"),
        dataCenter = None,
        extractEntityId = extractEntityId,
        extractShardId = extractShardId)
  }

  private val extractEntityId: ShardRegion.ExtractEntityId = {
    case msg@Tuple2(_, PaymentTransactionRequest(_, _, poiId, _)) => (poiId, msg)
  }

  private val extractShardId: ShardRegion.ExtractShardId = {
    case Tuple2(_, PaymentTransactionRequest(_, _, poiId, _)) => computeShardId(poiId)
    case ShardRegion.StartEntity(id) => computeShardId(id)
  }

  private val maximumNumberOfShards = 20

  private def computeShardId(entityId: ShardRegion.EntityId): ShardRegion.ShardId =
    (math.abs(entityId.hashCode) % maximumNumberOfShards).toString


}
