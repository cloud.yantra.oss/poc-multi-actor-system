/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.sales.commands

import java.util.UUID

case class PrintProcessingRequestProcessedCount() extends SaleCommand {

  override val uuid: UUID = UUID.randomUUID()

}
