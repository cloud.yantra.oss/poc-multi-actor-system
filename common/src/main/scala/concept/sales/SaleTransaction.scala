/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.sales

import java.time.LocalDateTime

import concept.sales.events.{SaleTransactionEvent, SaleTransactionInitiated, SaleTransactionProcessedSuccessfully, SaleTransactionProcessingFailed}

case class SaleTransaction(transactionId: Int,
                           paymentData: String,
                           time: LocalDateTime = LocalDateTime.now(),
                           status: SaleStatus = Initiated,
                           events: Seq[SaleTransactionEvent] = Nil)

object SaleTransaction {

  def apply(event: SaleTransactionInitiated): SaleTransaction =
    SaleTransaction(event.transactionId, event.paymentData, event.time, Initiated, Seq(event))

  def update(sale: SaleTransaction, event: SaleTransactionEvent): SaleTransaction = event match {
    case e: SaleTransactionProcessedSuccessfully =>
      sale.copy(status = Success, events = sale.events :+ e)
    case e: SaleTransactionProcessingFailed =>
      sale.copy(status = Failed, events = sale.events :+ e)
  }

}
