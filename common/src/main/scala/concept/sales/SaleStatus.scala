/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.sales

sealed trait SaleStatus

object Initiated extends SaleStatus

object Success extends SaleStatus

object Failed extends SaleStatus
