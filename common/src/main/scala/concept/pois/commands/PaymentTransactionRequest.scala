/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.pois.commands

import java.util.UUID

case class PaymentTransactionRequest(transactionId: Int,
                                     operator: String,
                                     poiId: String,
                                     paymentData: String) extends PoiCommand {

  override val uuid: UUID = UUID.randomUUID()

}
