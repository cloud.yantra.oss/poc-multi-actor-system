/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.pois.events

import java.util.UUID

import cloud.yantra.patterns.es.EntityId

case class PaymentTransactionFailed(transactionId: Int,
                                    operator: String,
                                    poiId: String,
                                    paymentData: String,
                                    reason: String) extends PoiEvent {

  override val uuid: UUID = UUID.randomUUID()

  override def entityId: Option[EntityId] = Some(EntityId(transactionId.toString))
}
