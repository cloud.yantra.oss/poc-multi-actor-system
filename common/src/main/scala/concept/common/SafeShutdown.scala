/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.common

/**
  * Message used by persistent actors to safely shutdown themselves
  */
case object SafeShutdown
