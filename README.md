# Akka based Microservice Architecture - Demo

# 1 - POI System

Simulating a POI device that processes payment

## Starting seed cluster
```bash
sbt -Dconfig.resource=poi-system.conf poi-system/run
``` 

## Starting worker node cluster
```bash
sbt -Dconfig.resource=poi-system-node1.conf poi-system/run
``` 

# 2 - Sale System

Simulating a payment routing system that accepts payment request, and routes to appropriate POI cluster

## Starting seed cluster
```bash
sbt -Dconfig.resource=sale-system.conf sale-system/run
``` 

## Starting worker node cluster
```bash
sbt -Dconfig.resource=sale-system-node1.conf sale-system/run
``` 

# 3 - Starting the Mock RESTful API application

A simple RESTful API service to receive api end point for accepting sale transaction and an SSE endpoints for responses.
```bash
sbt  mock-restful-api/clean mock-restful-api/run
```
If you have `curl`, then execute the following for sending sale request for an operator with name `operator1`
```bash
curl --header "Content-Type: application/json"   --request POST   --data '{}'   http://localhost:9000/sales/operator1/requests
```
To know the status of the processing.
```bash
curl -ivs --raw --request GET   http://localhost:9000/sales/operator1/responses
```
> Note : Steps for `1 - POI System` and `2 - Sale System` must be executed first.

# 4 - Starting the mock UI demonstrating SSE
A mock UI is created to demonstrate the display of SSE on a browser.

```bash
sbt -Dhttp.port=9001 mock-ui/clean mock-ui/run
```

One can view the response events of a sale by traversing to url `http://localhost:9001/views/operator1/events/`, where `operator1` is the operator that sent in the sale transactions. 


# 5 - Payment Generator

A simple application to simulate heavy generation of payments and send it to sale system cluster.gitlab.com:cloud.yantra.oss/poc-multi-actor-system.git

```bash
sbt -Dconfig.resource=source-system.conf  source-system/run
```
