/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

import java.time.{Duration, LocalDateTime}

import akka.actor.ActorSystem
import akka.cluster.Cluster
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.Publish
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import concept.sales.commands.{InitiateSaleTransaction, PrintProcessingRequestCount, PrintProcessingRequestProcessedCount}

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

object GeneratePaymentApp extends App {

  val config = ConfigFactory.load()
  //implicit val actorSystem = ActorSystem("GeneratePaymentApp", config)
  implicit val actorSystem = ActorSystem(config.getString("akka.actor.system"), config)
  println(s"ActorSystem => ${actorSystem}")
  Cluster.get(actorSystem).getSelfRoles.forEach( role => println(s"Role => `${role}`"))

  implicit val materializer: ActorMaterializer = ActorMaterializer()
  println(s"Getting actor system : ${config.getString("apps.sales.system")}")
  val saleSystemActorPath = config.getString("apps.sales.system")

  private val mediator = DistributedPubSub(actorSystem).mediator
  private val salesRequestTopic = config.getString("apps.sales.request-topic")

  private def saleSystemManager = actorSystem.actorSelection(saleSystemActorPath)

  val source1 = Source.tick(initialDelay = 0 second, interval = 1 second, tick = 1)

  var saleId = 0
  val saleIdSlot = 10
  val source = Source.tick(initialDelay = 0 second, interval = 1 second, tick = saleIdSlot)
  val sink = Sink.foreach[Int] { tick =>
    (1 to tick).foreach { increment =>
      saleId = saleId + increment
      val saleRef = if (saleId % 2 == 0) {
        s"Initiated a payment:for Nirvani: Sale ${saleId}"
      } else {
        s"Initiated a payment:for Nirviti: Sale ${saleId}"
      }
      val msg = InitiateSaleTransaction(saleId, "operator-dummy", saleRef)
      saleSystemManager ! msg
      mediator ! Publish(salesRequestTopic, msg)
      println(s"${saleRef}.........")
    }
  }

  val start = LocalDateTime.now
  val runnable = source.to(sink)
  runnable.run()

  val waitingPeriod = 1000L
  implicit val timeout = Timeout(waitingPeriod second)
  while (true) {
    val processedCount = Await.result(saleSystemManager ? PrintProcessingRequestProcessedCount, timeout.duration).asInstanceOf[Int]
    Thread.sleep(waitingPeriod)
    val requestCount = Await.result(saleSystemManager ? PrintProcessingRequestCount, timeout.duration).asInstanceOf[Int]
    val finish = LocalDateTime.now

    println("-------------------------------------------------------------------------------------------------------")
    println(s"Start -> ${start} | Finish -> ${finish}")
    println(s"Processing time -> ${Duration.between(start, finish).toSeconds}")
    println(s"Payment requested ${requestCount}, and processed ${processedCount}")
    println("-------------------------------------------------------------------------------------------------------")
    if (requestCount == processedCount | processedCount == Int.MaxValue) actorSystem.terminate()
  }
}
