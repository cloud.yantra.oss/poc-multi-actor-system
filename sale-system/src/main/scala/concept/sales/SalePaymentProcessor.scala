/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.sales

import java.time.LocalDateTime

import akka.actor.{ActorLogging, ActorRef, Props}
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.Publish
import akka.cluster.sharding.ClusterSharding
import akka.persistence.{PersistentActor, RecoveryCompleted, SnapshotOffer}
import concept.common.SafeShutdown
import concept.pois.commands.PaymentTransactionRequest
import concept.pois.events.{PaymentTransactionFailed, PaymentTransactionProcessed}
import concept.sales.commands.InitiateSaleTransaction
import concept.sales.events._

class SalePaymentProcessor(operator: String) extends PersistentActor with ActorLogging {

  val mediator = DistributedPubSub(context.system).mediator
  val publishTopic = s"responses/${operator}"

//  private val config = ConfigFactory.parseResources("sale-system.conf")
  private var state: SaleTransaction = _

  override def persistenceId: String = operator

  override def receiveRecover: Receive = {
    case e: SaleTransactionEvent =>
      updateState(e)
    case SnapshotOffer(_, snapshot: SaleTransaction) =>
      log.info(s"Snapshot offered. Snapshot: ${snapshot}")
      state = snapshot
    case RecoveryCompleted =>
      log.info(s"Recovery completed. Current state: ${state}")
  }

  override def receiveCommand: Receive = {

    case InitiateSaleTransaction(id, operator, paymentData) =>
      log.info(s"Payment with ${id} and reference ${paymentData} made by processor ${self.path}, with id as ${operator}")
      //Send the payment to POI manager for getting processed
      log.info(s"Poi [${(id % 13).toString}] device manager -> ${getPoiDeviceManager.path}")
      //if (state == null) {
        val event = SaleTransactionInitiated(id, operator, paymentData, LocalDateTime.now())
        persist(event)(updateState)
        mediator ! Publish(publishTopic, event)
        context.parent ! event
        getPoiDeviceManager ! Tuple2(self, PaymentTransactionRequest(id, operator, (id % 13).toString, paymentData))

      /*} else {
        val event = SaleTransactionIdAlreadyUsed(id)
        mediator ! Publish(publishTopic, event)
        context.parent ! event
        self ! SafeShutdown
      }*/
    case ptp@PaymentTransactionProcessed(transactionId, operator, poiId, paymentData, poiTransactionId, time) =>
      log.info(s"Payment processed -> ${ptp}")
      val event = SaleTransactionProcessedSuccessfully(transactionId, operator, poiId, paymentData, poiTransactionId, time)
      persist(event)(updateState)
      mediator ! Publish(publishTopic, event)
      context.parent ! event
      self ! SafeShutdown

    case ptp@PaymentTransactionFailed(transactionId, operator, poiId, paymentData, reason) =>
      log.info(s"Payment processed -> ${ptp}")
      val event = SaleTransactionProcessingFailed(transactionId, operator, poiId, paymentData, reason)
      persist(event)(updateState)
      mediator ! Publish(publishTopic, event)
      context.parent ! event
      self ! SafeShutdown

    case SafeShutdown =>
      context.stop(self)

    case _ =>
      log.error(s"C'mon maccha send PaymentProcessor ${operator} proper transaction")
  }

  /**
    * Poi device manager to get the payment processed
    *
    * @return
    */
//  private val getPoiDeviceManager: ActorSelection = context.actorSelection(config.getString("apps.poi-system"))
  private def getPoiDeviceManager: ActorRef = ClusterSharding(context.system).shardRegion("PoiDevice")

  private def updateState(event: SaleTransactionEvent): Unit = event match {
    case initialization: SaleTransactionInitiated =>
      state = SaleTransaction(initialization)
    case e: SaleTransactionEvent =>
      state = SaleTransaction.update(state, e)
  }

}

object SalePaymentProcessor {
  def props(operator: String): Props = Props[SalePaymentProcessor](new SalePaymentProcessor(operator))
}
