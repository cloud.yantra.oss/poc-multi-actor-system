/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.sales

import akka.actor.{Actor, ActorLogging, Props}
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.{Subscribe, SubscribeAck, Unsubscribe}
import concept.common.SafeShutdown
import concept.sales.commands.{InitiateSaleTransaction, PrintProcessingRequestCount, PrintProcessingRequestProcessedCount}
import concept.sales.events.{SaleTransactionIdAlreadyUsed, SaleTransactionProcessedSuccessfully, SaleTransactionProcessingFailed}

class SalePaymentManager extends Actor with ActorLogging {
  var request: Int = 0
  var processed: Int = 0
  var failed: Int = 0


  val mediator = DistributedPubSub(context.system).mediator
  val subscribeTopic = "sale-system/sales"


  override def preStart(): Unit = {
    mediator ! Subscribe(subscribeTopic, self)
    log.info(s"${self} subscribed to topic ${subscribeTopic} using mediator ${mediator}")
    super.preStart()
  }

  override def receive: Receive = {
    case sale@InitiateSaleTransaction(pid, operator, _) =>
      log.info(s"Being processed by Sale System at ${self.path}")
      val pp = context.actorOf(SalePaymentProcessor.props(operator), s"payment-processor-${pid}")
      request += 1
      pp ! sale

    case SaleTransactionProcessedSuccessfully(transactionId, _, poiId, _, poiTransactionId, time) =>
      log.info(s"Successfully processed payment ${transactionId}, by POS device ${poiId} at time ${time}, with POI transaction id ${poiTransactionId}")
      processed += 1

    case SaleTransactionIdAlreadyUsed(transactionId) =>
      log.warning(s"Transaction id ${transactionId} is duplicate")

    case SaleTransactionProcessingFailed(transactionId, _, poiId, _, reason) =>
      log.warning(s"Processing of payment with transactionId:${transactionId}, failed at ${poiId}, for reason ${reason}")
      failed += 1

    case PrintProcessingRequestCount =>
      log.info(s"Number of processing request -> ${request}")
      sender ! request

    case PrintProcessingRequestProcessedCount =>
      log.info(s"Number of request processed -> ${processed}")
      sender ! processed

    case SafeShutdown =>
      context.stop(self)

    case SubscribeAck(Subscribe(topic, None, `self`)) ⇒
      log.info(s"Subscribing to topic [${topic}]")

    case msg  =>
      log.info(s"Send me some stuff please ${sender.path}")
      log.info(s"Received ${msg.getClass} ")
  }

  override def postStop(): Unit = {
    mediator ! Unsubscribe(subscribeTopic, self)
    super.postStop()
  }
}

object SalePaymentManager {
  def props: Props = Props[SalePaymentManager](new SalePaymentManager())
}
