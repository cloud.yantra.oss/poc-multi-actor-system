/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

import akka.actor.ActorSystem
import akka.cluster.Cluster
import akka.cluster.sharding.ClusterSharding
import com.typesafe.config.ConfigFactory
import concept.sales.SalePaymentManager
import concept.shards.PoiDeviceShard

object SaleActorSystemApp extends App {
  val config = ConfigFactory.load()
  val actorSystem = ActorSystem.create(config.getString("akka.actor.system"), config)
  println(s"ActorSystem => ${actorSystem}")
  Cluster.get(actorSystem).getSelfRoles.forEach(role => println(s"Role => `${role}`"))

  val saleSystemManagerId = "sale-system-manager"
  println(s"Creating SaleSystem ${saleSystemManagerId}")
  val saleSystemManager = actorSystem.actorOf(SalePaymentManager.props, saleSystemManagerId)
  println(s"SaleSystem manager created at -> ${saleSystemManager} | ${saleSystemManager.path}")

  PoiDeviceShard.startPoiDeviceShardingProxy(actorSystem)

  println(s"All shards in the cluster are ......................")
  ClusterSharding(actorSystem).shardTypeNames.foreach { t =>
    println(s"Avaliable shard type is [${t}]")
  }
  println(s" Path to shard region [${ClusterSharding(actorSystem).shardRegion("PoiDevice").path}]")

}
