import play.sbt.routes.RoutesKeys

name := "poc-multi-actor-system"

ThisBuild / organization := "cloud.yantra.oss"
ThisBuild / version := "0.1"
ThisBuild / scalaVersion := "2.12.6"
ThisBuild / scalacOptions ++= CompilerFlags.compilerFlags
ThisBuild / scalacOptions in(Compile, console) ~= CompilerFlags.filterExcludedReplOptions
ThisBuild / resolvers +=
  "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

lazy val akkaVersion = "2.5.23"
lazy val akkaHttpVersion = "10.0.8"
lazy val scalatestVersion = "3.0.5"
lazy val cqrsPatternVersion = "0.2.0-SNAPSHOT"
lazy val playVersion = "2.6.20"

lazy val serviceLibraryDependencies = Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion withSources(),
  "com.typesafe.akka" %% "akka-persistence" % akkaVersion withSources(),
  "com.typesafe.akka" %% "akka-remote" % akkaVersion withSources(),
  "com.typesafe.akka" %% "akka-cluster" % akkaVersion withSources(),
  "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion withSources(),
  "com.typesafe.akka" %% "akka-cluster-sharding" % akkaVersion withSources(),
  "org.fusesource.leveldbjni" % "leveldbjni-all" % "1.8" withSources(),
  "org.scalatest" %% "scalatest" % scalatestVersion % "test",
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % "test" withSources()
)

lazy val testing = Seq(
  "org.fusesource.leveldbjni" % "leveldbjni-all" % "1.8" withSources(),
  "org.scalatest" %% "scalatest" % scalatestVersion % "test",
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % "test" withSources()
)

lazy val mockRestfulApi = Seq(
  "com.typesafe.play" %% "play-guice" % playVersion withSources(),
  "com.typesafe.play" %% "play-json" % "2.6.10" withSources(),
  "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion withSources(),
  "com.typesafe.akka" %% "akka-stream" % akkaVersion withSources()
)

//Common setting
lazy val commonSetting = Seq(
  scalastyleConfig := (ThisBuild / baseDirectory) (_ / scalaCheckstyle).value
)

//Projects
lazy val common = project
  .settings(
    name := "common",
    libraryDependencies ++= Seq(
      "cloud.yantra.oss" %% "microservice-cqrs-pattern" % cqrsPatternVersion withSources() exclude("org.slf4j", "slf4j-simple"),
      "com.typesafe.akka" %% "akka-cluster" % akkaVersion withSources(),
      "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion withSources(),
      "com.typesafe.akka" %% "akka-cluster-sharding" % akkaVersion withSources()
    ),
    commonSetting
  )
lazy val `sale-system` = project
  .settings(
    name := "sale-system",
    libraryDependencies ++= serviceLibraryDependencies,
    commonSetting
  ).dependsOn(common)
lazy val `poi-system` = project
  .settings(
    name := "poi-system",
    libraryDependencies ++= serviceLibraryDependencies,
    commonSetting
  ).dependsOn(common)
lazy val `source-system` = project
  .settings(
    name := "source-system",
    libraryDependencies ++= serviceLibraryDependencies,
    commonSetting
  ).dependsOn(common)

/** ****************************************************************************
  * Mock RESTful API using Play framework
  * ****************************************************************************
  */
//lazy val `mock-restful-api` = (project in file("mock-restful-api")).settings(
lazy val `mock-restful-api` = project.settings(
  name := "mock-restful-api",
  routesGenerator := InjectedRoutesGenerator,
  TwirlKeys.templateImports := Seq.empty,
  RoutesKeys.routesImport := Seq.empty,
  libraryDependencies ++= mockRestfulApi,
  PlayKeys.playMonitoredFiles ++= (sourceDirectories in(Compile, TwirlKeys.compileTemplates)).value,
  commonSetting
).dependsOn(common)
  .enablePlugins(PlayScala)
  .disablePlugins(PlayLayoutPlugin)

/** ****************************************************************************
  * Mock UI for SSE using Play framework
  * ****************************************************************************
  */
lazy val `mock-ui` = project.settings(
  name := "mock-ui",
  routesGenerator := InjectedRoutesGenerator,
  TwirlKeys.templateImports := Seq.empty,
  RoutesKeys.routesImport := Seq.empty,
  libraryDependencies ++= mockRestfulApi,
  PlayKeys.playMonitoredFiles ++= (sourceDirectories in(Compile, TwirlKeys.compileTemplates)).value,
  commonSetting
).dependsOn(common)
  .enablePlugins(PlayScala)
  .enablePlugins(PlayLayoutPlugin)

lazy val `poc-multi-actor-system` = project
  .aggregate(
    common,
    `sale-system`,
    `poi-system`,
    `source-system`,
    `mock-restful-api`
  )


/** ****************************************************************************
  * CI : Scala Checkstyle
  * Ref: http://www.scalastyle.org/sbt.html
  * Usage: sbt scalastyle
  * ****************************************************************************
  */
lazy val scalaCheckstyle = "ci/checkstyle/scala/scalastyle-config.xml"
scalastyleConfig := (ThisBuild / baseDirectory) (_ / scalaCheckstyle).value
scalastyleFailOnWarning := true

/** ****************************************************************************
  * CI : Pipeline Simulation
  * Usage: sbt pipeline-ci
  * ****************************************************************************
  */
commands += Command.command("pipeline-ci") { state =>
  "clean" ::
    "compile" ::
    "test" ::
    "common/scalastyle" ::
    "sale-system/scalastyle" ::
    "poi-system/scalastyle" ::
    "source-system/scalastyle" ::
    "mock-restful-api/scalastyle" ::
    "mock-ui/scalastyle" ::
    state
}
